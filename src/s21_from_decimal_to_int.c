#include "s21_decimal.h"

int s21_from_decimal_to_int(s21_decimal src, int *dst) {
  int error = 0;
  s21_decimal temp = {0};
  s21_truncate(src, &temp);
  if (temp.bits[1] || temp.bits[2]) {
    error = CONVERT_ERROR;
  } else {
    *dst = temp.bits[0];
  }
  if (get_sign(src)) {
    *dst = *dst * (-1);
  }
  return error;
}