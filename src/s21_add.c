#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "s21_decimal.h"

int s21_add(s21_decimal value_1, s21_decimal value_2, s21_decimal *result) {
  int sign1 = get_sign(value_1), sign2 = get_sign(value_2),
      need_to_change_sign = 0;
  s21_long_decimal num1 = {0}, num2 = {0}, result_long = {0};
  decimal_to_long_decimal(value_1, &num1);
  set_bit_long(&num1, 223, 0);
  decimal_to_long_decimal(value_2, &num2);
  set_bit_long(&num2, 223, 0);
  int first_greater_second = long_greater(num1, num2);
  if (sign1 == sign2) {
    long_sum(num1, num2, &result_long);
    if (sign1 == 1) {
      need_to_change_sign = 1;
    }
  } else if (sign1 == 0 && sign2 == 1) {
    if (first_greater_second == 1) {
      long_sub(num1, num2, &result_long);
    } else {
      long_sub(num2, num1, &result_long);
      need_to_change_sign = 1;
    }
  } else if (sign1 == 1 && sign2 == 0) {
    if (first_greater_second == 1) {
      long_sub(num1, num2, &result_long);
      need_to_change_sign = 1;
    } else {
      long_sub(num2, num1, &result_long);
    }
  }
  if (need_to_change_sign) {
    set_bit_long(&result_long, 223, 1);
  }
  int ret = long_decimal_to_decimal(result_long, result);

  return ret;
}

int long_sum(s21_long_decimal value1, s21_long_decimal value2,
             s21_long_decimal *result) {
  if (get_exponent_long(value1) != get_exponent_long(value2)) {
    set_to_biggest_exponent(&value1, &value2);
  }
  s21_long_decimal carry = {0}, temp_result = {0};
  for (int i = 0; i < 6; i++) {
    temp_result.bits[i] = value1.bits[i] ^ value2.bits[i];
    carry.bits[i] = value1.bits[i] & value2.bits[i];
  }
  long_shift_left(&carry, 1);
  while (check_for_zero_long(carry) == 0) {
    s21_long_decimal temp_carry = {0};
    memcpy(&temp_carry, &carry, sizeof(s21_long_decimal));
    for (int i = 0; i < 6; i++) {
      temp_carry.bits[i] = temp_result.bits[i] & carry.bits[i];
      temp_result.bits[i] ^= carry.bits[i];
    }
    long_shift_left(&temp_carry, 1);
    memcpy(&carry, &temp_carry, sizeof(s21_long_decimal));
  }
  memcpy(result, &temp_result, sizeof(s21_long_decimal));
  set_exponent_long(result, get_exponent_long(value1));
  return 0;
}