#include "s21_decimal.h"

int s21_truncate(s21_decimal value, s21_decimal *result) {
  s21_decimal temp = {{0, 0, 0, 0}};
  *result = temp;
  memcpy(&temp, &value, sizeof(s21_decimal));
  int exp1 = get_exponent(value);
  int start_index = get_starting_index(value);
  if ((start_index <= 31 && exp1 >= 10) || (start_index <= 63 && exp1 >= 20)) {
    // мы ничего не делаем, должен получится ноль
  } else if (exp1) {
    int sign1 = get_sign(value);
    set_exponent(&temp, 0);
    set_bit(&temp, 127, 0);
    s21_decimal zero = {{0, 0, 0, 0}};
    s21_decimal tens[9] = {
        {{10, 0, 0, 0}},       {{100, 0, 0, 0}},       {{1000, 0, 0, 0}},
        {{10000, 0, 0, 0}},    {{100000, 0, 0, 0}},    {{1000000, 0, 0, 0}},
        {{10000000, 0, 0, 0}}, {{100000000, 0, 0, 0}}, {{1000000000, 0, 0, 0}}};
    while (exp1 || s21_is_equal(temp, zero) == 1) {
      if (exp1 >= 9) {
        s21_div(temp, tens[8], result);
        temp = *result;
        // memcpy(&temp, &result, sizeof(s21_decimal));
        exp1 -= 9;
      } else {
        s21_div(temp, tens[exp1 - 1], result);
        memcpy(&temp, &result, sizeof(s21_decimal));
        exp1 = 0;
      }
    }
    if (sign1) {
      set_bit(result, 127, 1);
    }
  } else {
    memcpy(result, &value, sizeof(s21_decimal));
  }
  return 0;
}
