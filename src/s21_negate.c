#include "s21_decimal.h"

int s21_negate(s21_decimal value, s21_decimal *result) {
  bool sign = get_sign(value);
  *result = value;
  if (sign == 1) {
    set_bit(result, 127, 0);
  } else {
    set_bit(result, 127, 1);
  }
  return 0;
}
