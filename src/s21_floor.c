#include "s21_decimal.h"

int s21_floor(s21_decimal value, s21_decimal *result) {
  int exp1 = get_exponent(value);
  if (exp1) {
    s21_truncate(value, result);
    s21_decimal one = {{1, 0, 0, 0}};
    if (get_sign(*result)) {
      s21_sub(*result, one, result);
    }
  } else {
    memcpy(result, &value, sizeof(s21_decimal));
  }
  return 0;
}