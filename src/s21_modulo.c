#include "s21_decimal.h"

int long_div(s21_long_decimal num1, s21_long_decimal num2,
             s21_long_decimal *result) {  // не трудив, просто див
  s21_long_decimal remainder = {0}, long_multiplier = {0}, divider = {0};
  *result = remainder;
  if (long_eq(num1, *result)) return OK;
  if (long_eq(num2, *result)) return DIV_0;
  bool sign1 = get_sign_long(num1), sign2 = get_sign_long(num2);
  set_bit_long(&num1, 223, 0);
  set_bit_long(&num2, 223, 0);
  int exp1 = get_exponent_long(num1), exp2 = get_exponent_long(num2),
      exp_mul = exp1 >= exp2 ? exp1 : exp2;
  if (exp_mul != 0) {
    float multiplier_fl = pow(10, exp_mul);
    s21_decimal mul_short = {0};
    s21_from_float_to_decimal(multiplier_fl, &mul_short);
    decimal_to_long_decimal(mul_short, &long_multiplier);
    mul_long(num1, long_multiplier, &num1);
    mul_long(num2, long_multiplier, &num2);
  }
  remainder = num1;
  divider = num2;
  int start_index1 = get_starting_index_long(remainder),
      start_index2 = get_starting_index_long(divider);
  long_shift_left(&divider, start_index1 - start_index2);
  int iter_counter = 0;
  while (long_greater(divider, num2) || long_eq(divider, num2)) {
    long_shift_left(result, 1);
    if (long_greater(remainder, divider) || long_eq(remainder, divider)) {
      set_bit_long(result, 0, 1);
      long_sub(remainder, divider, &remainder);
    }
    long_shift_right(&divider, 1);
    iter_counter++;
  }
  if (start_index1 - start_index2 + 1 > iter_counter) {
    long_shift_left(result, start_index1 - start_index2 + 1 - iter_counter);
  }
  if (sign1 ^ sign2) {
    set_bit_long(result, 223, 1);
  }
  return 0;
}

// int long_div(s21_long_decimal num1, s21_long_decimal num2, s21_long_decimal
// *result) {
//   int sign1 = get_sign_long(num1), sign2 = get_sign_long(num2);
//   set_bit_long(&num1, 223, 0);
//   set_bit_long(&num2, 223, 0);
//   int exp1 = get_exponent_long(num1), exp2 = get_exponent_long(num2);
// }

int s21_mod(s21_decimal value_1, s21_decimal value_2, s21_decimal *result) {
  s21_decimal zero = {0};
  if (s21_is_equal(value_2, zero) == 1) {
    return DIV_0;
  }
  s21_long_decimal num1 = {0}, num2 = {0}, result_long = {0};
  decimal_to_long_decimal(value_1, &num1);
  decimal_to_long_decimal(value_2, &num2);
  long_div(num1, num2, &result_long);
  s21_long_decimal to_sub = {0};
  mul_long(result_long, num2, &to_sub);
  long_sub(num1, to_sub, &result_long);
  long_decimal_to_decimal(result_long, result);

  return 0;
}
