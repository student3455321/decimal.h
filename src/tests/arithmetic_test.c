#include "tests.h"

START_TEST(s21_add_test) {
  {
    s21_decimal value_1 = {{13, 0, 0, 0}};
    set_exponent(&value_1, 1);
    s21_decimal value_2 = {{286, 0, 0, 0}};
    set_exponent(&value_2, 2);
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{416, 0, 0, 0}};
    set_exponent(&check, 2);
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{13, 0, 0, 0}};
    set_exponent(&value_1, 1);
    s21_decimal value_2 = {{286, 0, 0, 0}};
    set_exponent(&value_2, 2);
    value_2.bits[3] |= 1UL << 31;
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{156, 0, 0, 0}};
    check.bits[3] |= 1UL << 31;
    set_exponent(&check, 2);
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{1364, 0, 0, 0}};
    value_1.bits[3] |= 1UL << 31;
    set_exponent(&value_1, 1);
    s21_decimal value_2 = {{286, 0, 0, 0}};
    set_exponent(&value_2, 2);
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{13354, 0, 0, 0}};
    check.bits[3] |= 1UL << 31;
    set_exponent(&check, 2);
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{286, 0, 0, 0}};
    set_exponent(&value_1, 2);
    s21_decimal value_2 = {{13, 0, 0, 0}};
    set_exponent(&value_2, 1);
    value_2.bits[3] |= 1UL << 31;
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{156, 0, 0, 0}};
    set_exponent(&check, 2);
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0}};
    value_2.bits[3] |= 1UL << 31;
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{0, 0, 0, 0}};
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }

  {
    s21_decimal value_1 = {{0xFFFFFFFF, 0xFFFFFFFF, 0, 0}};
    set_exponent(&value_1, 1);
    s21_decimal value_2 = {{2, 0, 0, 0}};
    set_exponent(&value_2, 1);
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{0x1, 0x0, 0x1, 0}};
    set_exponent(&check, 1);
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }

  {
    s21_decimal value_1 = {{13, 0, 0, 0}};
    set_exponent(&value_1, 2);
    s21_decimal value_2 = {{286, 0, 0, 0}};
    set_exponent(&value_2, 1);
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{2873, 0, 0, 0}};
    set_exponent(&check, 2);
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }

  {
    s21_decimal value_1 = {{1, 0, 0, 0}};
    s21_decimal value_2 = {{0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0}};
    s21_decimal result = {{0, 0, 0, 0}};
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(return_value, 1);
  }

  {
    s21_decimal value_1 = {{13, 0, 0, 0}};
    set_exponent(&value_1, 2);
    value_1.bits[3] |= 1UL << 31;
    s21_decimal value_2 = {{286, 0, 0, 0}};
    set_exponent(&value_2, 1);
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{2847, 0, 0, 0}};
    set_exponent(&check, 2);
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, 0);
  }

  {
    s21_decimal value_1 = {{13, 0, 0, 0}};
    set_exponent(&value_1, 1);
    value_1.bits[3] |= 1UL << 31;
    s21_decimal value_2 = {{286, 0, 0, 0}};
    set_exponent(&value_2, 2);
    value_2.bits[3] |= 1UL << 31;
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{416, 0, 0, 0}};
    set_exponent(&check, 2);
    check.bits[3] |= 1UL << 31;
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }

  {
    s21_decimal value_1 = {{1614, 0, 0, 0}};
    set_exponent(&value_1, 3);
    s21_decimal value_2 = {{46071, 0, 0, 0}};
    set_exponent(&value_2, 2);
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{462324, 0, 0, 0}};
    set_exponent(&check, 3);
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0}};
    set_exponent(&value_1, 1);
    s21_decimal value_2 = {{1, 0, 0, 0}};
    s21_decimal result = {{0, 0, 0, 0}};
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(return_value, 0);
  }
}
END_TEST

START_TEST(s21_sub_test) {
  {
    s21_decimal value_1 = {{13, 0, 0, 0}};
    set_exponent(&value_1, 1);
    value_1.bits[3] |= 1UL << 31;
    s21_decimal value_2 = {{286, 0, 0, 0}};
    set_exponent(&value_2, 2);
    value_2.bits[3] |= 1UL << 31;
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{156, 0, 0, 0}};
    set_exponent(&check, 2);
    int return_value = s21_sub(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{0, 13, 0, 0}};
    set_exponent(&value_1, 1);
    value_1.bits[3] |= 1UL << 31;
    s21_decimal value_2 = {{0, 286, 0, 0}};
    set_exponent(&value_2, 2);
    value_2.bits[3] |= 1UL << 31;
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{0, 156, 0, 0}};
    set_exponent(&check, 2);
    int return_value = s21_sub(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{286, 0, 0, 0}};
    set_exponent(&value_1, 2);
    value_1.bits[3] |= 1UL << 31;
    s21_decimal value_2 = {{13, 0, 0, 0}};
    set_exponent(&value_2, 1);
    value_2.bits[3] |= 1UL << 31;
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{156, 0, 0, 0}};
    check.bits[3] |= 1UL << 31;
    set_exponent(&check, 2);
    int return_value = s21_sub(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{13, 0, 0, 0}};
    set_exponent(&value_1, 1);
    value_1.bits[3] |= 1UL << 31;
    s21_decimal value_2 = {{286, 0, 0, 0}};
    set_exponent(&value_2, 2);
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{416, 0, 0, 0}};
    set_exponent(&check, 2);
    check.bits[3] |= 1UL << 31;
    int return_value = s21_sub(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }

  {
    s21_decimal value_1 = {{0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0}};
    value_1.bits[3] |= 1UL << 31;
    s21_decimal value_2 = {{2, 0, 0, 0}};
    s21_decimal result = {{0, 0, 0, 0}};
    int return_value = s21_sub(value_1, value_2, &result);
    ck_assert_int_eq(return_value, 2);
  }

  {
    s21_decimal value_1 = {{0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0}};
    value_1.bits[3] |= 1UL << 31;
    s21_decimal value_2 = {{2, 0, 0, 0}};
    value_2.bits[3] |= 1UL << 31;
    s21_decimal result = {{0, 0, 0, 0}};
    int return_value = s21_add(value_1, value_2, &result);
    ck_assert_int_eq(return_value, 2);
  }

  {
    s21_decimal value_1 = {{64071, 0, 0, 0}};
    set_exponent(&value_1, 4);
    s21_decimal value_2 = {{5919, 0, 0, 0}};
    set_exponent(&value_2, 1);
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{5854929, 0, 0, 0}};
    set_exponent(&check, 4);
    check.bits[3] |= 1UL << 31;
    int return_value = s21_sub(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
}
END_TEST

START_TEST(s21_mul_test) {
  {
    s21_decimal value_1 = {{5, 0, 0, 0}};
    s21_decimal value_2 = {{7, 0, 0, 0}};
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{35, 0, 0, 0}};
    int return_value = s21_mul(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }

  {
    s21_decimal value_1 = {{0xFFFFFFFF, 0, 0, 0}};
    s21_decimal value_2 = {{0xFFFFFFFF, 0, 0, 0}};
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{1, 0xFFFFFFFE, 0, 0}};
    int return_value = s21_mul(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }

  {
    s21_decimal value_1 = {{0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{2, 0, 0, 0}};
    s21_decimal result = {{0, 0, 0, 0}};
    int return_value = s21_mul(value_1, value_2, &result);
    ck_assert_int_eq(return_value, 1);
  }

  {
    s21_decimal value_1 = {{123456u, 123u, 0, 0}};
    s21_decimal value_2 = {{654321u, 654u, 0, 0}};
    value_2.bits[3] |= 1UL << 31;
    set_exponent(&value_1, 2);
    set_exponent(&value_2, 3);
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{0xcedabe40, 0x99c0c5d, 0x13a3a, 0x80050000}};
    int return_value = s21_mul(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }

  {
    s21_decimal value_1 = {{0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{2, 0, 0, 0}};
    value_2.bits[3] |= 1UL << 31;
    s21_decimal result = {{0, 0, 0, 0}};
    int return_value = s21_mul(value_1, value_2, &result);
    ck_assert_int_eq(return_value, TOO_SMALL);
  }

  {
    s21_decimal value_1 = {{17, 0, 0, 0}};
    value_1.bits[3] |= 1UL << 31;
    s21_decimal value_2 = {{0, 0, 0, 0}};
    s21_decimal result = {{0, 0, 0, 0}};
    s21_decimal check = {{0, 0, 0, 0}};
    int return_value = s21_mul(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, origin = {0}, result = {0};
    int value_type_result = 0, value_type_origin = 0;
    // src1 = 2;
    // src2 = 3;
    src1.bits[0] = 0b00000000000000000000000000000010;
    src1.bits[1] = 0b00000000000000000000000000000000;
    src1.bits[2] = 0b00000000000000000000000000000000;
    src1.bits[3] = 0b00000000000000000000000000000000;
    src2.bits[0] = 0b00000000000000000000000000000011;
    src2.bits[1] = 0b00000000000000000000000000000000;
    src2.bits[2] = 0b00000000000000000000000000000000;
    src2.bits[3] = 0b00000000000000000000000000000000;
    value_type_result = s21_mul(src1, src2, &result);
    value_type_origin = 0;
    origin.bits[0] = 0b00000000000000000000000000000110;
    origin.bits[1] = 0b00000000000000000000000000000000;
    origin.bits[2] = 0b00000000000000000000000000000000;
    origin.bits[3] = 0b00000000000000000000000000000000;
    ck_assert_int_eq(origin.bits[3], result.bits[3]);
    ck_assert_int_eq(origin.bits[2], result.bits[2]);
    ck_assert_int_eq(origin.bits[1], result.bits[1]);
    ck_assert_int_eq(origin.bits[0], result.bits[0]);
    ck_assert_int_eq(value_type_result, value_type_origin);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, origin = {0}, result = {0};
    int value_type_result = 0, value_type_origin = 0;
    // src1 = 3;
    // src2 = 2;
    src1.bits[0] = 0b00000000000000000000000000000011;
    src1.bits[1] = 0b00000000000000000000000000000000;
    src1.bits[2] = 0b00000000000000000000000000000000;
    src1.bits[3] = 0b00000000000000000000000000000000;
    src2.bits[0] = 0b00000000000000000000000000000010;
    src2.bits[1] = 0b00000000000000000000000000000000;
    src2.bits[2] = 0b00000000000000000000000000000000;
    src2.bits[3] = 0b00000000000000000000000000000000;
    value_type_result = s21_mul(src1, src2, &result);
    value_type_origin = 0;
    origin.bits[0] = 0b00000000000000000000000000000110;
    origin.bits[1] = 0b00000000000000000000000000000000;
    origin.bits[2] = 0b00000000000000000000000000000000;
    origin.bits[3] = 0b00000000000000000000000000000000;
    ck_assert_int_eq(origin.bits[3], result.bits[3]);
    ck_assert_int_eq(origin.bits[2], result.bits[2]);
    ck_assert_int_eq(origin.bits[1], result.bits[1]);
    ck_assert_int_eq(origin.bits[0], result.bits[0]);
    ck_assert_int_eq(value_type_result, value_type_origin);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, origin = {0}, result = {0};
    int value_type_result = 0, value_type_origin = 0;
    // src1 = 0;
    // src2 = 3;
    src1.bits[0] = 0b00000000000000000000000000000000;
    src1.bits[1] = 0b00000000000000000000000000000000;
    src1.bits[2] = 0b00000000000000000000000000000000;
    src1.bits[3] = 0b00000000000000000000000000000000;
    src2.bits[0] = 0b00000000000000000000000000000011;
    src2.bits[1] = 0b00000000000000000000000000000000;
    src2.bits[2] = 0b00000000000000000000000000000000;
    src2.bits[3] = 0b00000000000000000000000000000000;
    value_type_result = s21_mul(src1, src2, &result);
    value_type_origin = 0;
    origin.bits[0] = 0b00000000000000000000000000000000;
    origin.bits[1] = 0b00000000000000000000000000000000;
    origin.bits[2] = 0b00000000000000000000000000000000;
    origin.bits[3] = 0b00000000000000000000000000000000;
    ck_assert_int_eq(origin.bits[3], result.bits[3]);
    ck_assert_int_eq(origin.bits[2], result.bits[2]);
    ck_assert_int_eq(origin.bits[1], result.bits[1]);
    ck_assert_int_eq(origin.bits[0], result.bits[0]);
    ck_assert_int_eq(value_type_result, value_type_origin);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, origin = {0}, result = {0};
    int value_type_result = 0, value_type_origin = 0;
    // src1 = 2;
    // src2 = 0;
    src1.bits[0] = 0b00000000000000000000000000000010;
    src1.bits[1] = 0b00000000000000000000000000000000;
    src1.bits[2] = 0b00000000000000000000000000000000;
    src1.bits[3] = 0b00000000000000000000000000000000;
    src2.bits[0] = 0b00000000000000000000000000000000;
    src2.bits[1] = 0b00000000000000000000000000000000;
    src2.bits[2] = 0b00000000000000000000000000000000;
    src2.bits[3] = 0b00000000000000000000000000000000;
    value_type_result = s21_mul(src1, src2, &result);
    value_type_origin = 0;
    origin.bits[0] = 0b00000000000000000000000000000000;
    origin.bits[1] = 0b00000000000000000000000000000000;
    origin.bits[2] = 0b00000000000000000000000000000000;
    origin.bits[3] = 0b00000000000000000000000000000000;
    ck_assert_int_eq(origin.bits[3], result.bits[3]);
    ck_assert_int_eq(origin.bits[2], result.bits[2]);
    ck_assert_int_eq(origin.bits[1], result.bits[1]);
    ck_assert_int_eq(origin.bits[0], result.bits[0]);
    ck_assert_int_eq(value_type_result, value_type_origin);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, origin = {0}, result = {0};
    int value_type_result = 0, value_type_origin = 0;
    // src1 = 18446744073709551615;
    // src2 = 1;
    src1.bits[0] = 0b11111111111111111111111111111111;
    src1.bits[1] = 0b11111111111111111111111111111111;
    src1.bits[2] = 0b00000000000000000000000000000000;
    src1.bits[3] = 0b00000000000000000000000000000000;
    src2.bits[0] = 0b00000000000000000000000000000001;
    src2.bits[1] = 0b00000000000000000000000000000000;
    src2.bits[2] = 0b00000000000000000000000000000000;
    src2.bits[3] = 0b00000000000000000000000000000000;
    value_type_result = s21_mul(src1, src2, &result);
    value_type_origin = 0;
    origin.bits[0] = 0b11111111111111111111111111111111;
    origin.bits[1] = 0b11111111111111111111111111111111;
    origin.bits[2] = 0b00000000000000000000000000000000;
    origin.bits[3] = 0b00000000000000000000000000000000;
    ck_assert_int_eq(origin.bits[3], result.bits[3]);
    ck_assert_int_eq(origin.bits[2], result.bits[2]);
    ck_assert_int_eq(origin.bits[1], result.bits[1]);
    ck_assert_int_eq(origin.bits[0], result.bits[0]);
    ck_assert_int_eq(value_type_result, value_type_origin);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, origin = {0}, result = {0};
    int value_type_result = 0, value_type_origin = 0;
    // src1 = 0;
    // src2 = 0;
    src1.bits[0] = 0b00000000000000000000000000000000;
    src1.bits[1] = 0b00000000000000000000000000000000;
    src1.bits[2] = 0b00000000000000000000000000000000;
    src1.bits[3] = 0b00000000000000000000000000000000;
    src2.bits[0] = 0b00000000000000000000000000000000;
    src2.bits[1] = 0b00000000000000000000000000000000;
    src2.bits[2] = 0b00000000000000000000000000000000;
    src2.bits[3] = 0b00000000000000000000000000000000;
    value_type_result = s21_mul(src1, src2, &result);
    value_type_origin = 0;
    origin.bits[0] = 0b00000000000000000000000000000000;
    origin.bits[1] = 0b00000000000000000000000000000000;
    origin.bits[2] = 0b00000000000000000000000000000000;
    origin.bits[3] = 0b00000000000000000000000000000000;
    ck_assert_int_eq(origin.bits[3], result.bits[3]);
    ck_assert_int_eq(origin.bits[2], result.bits[2]);
    ck_assert_int_eq(origin.bits[1], result.bits[1]);
    ck_assert_int_eq(origin.bits[0], result.bits[0]);
    ck_assert_int_eq(value_type_result, value_type_origin);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, origin = {0}, result = {0};
    int value_type_result = 0, value_type_origin = 0;
    // src1 = 79228162514264337593543950335;
    // src2 = -1;
    src1.bits[0] = 0b11111111111111111111111111111111;
    src1.bits[1] = 0b11111111111111111111111111111111;
    src1.bits[2] = 0b11111111111111111111111111111111;
    src1.bits[3] = 0b00000000000000000000000000000000;
    src2.bits[0] = 0b00000000000000000000000000000001;
    src2.bits[1] = 0b00000000000000000000000000000000;
    src2.bits[2] = 0b00000000000000000000000000000000;
    src2.bits[3] = 0b10000000000000000000000000000000;
    value_type_result = s21_mul(src1, src2, &result);
    value_type_origin = 0;
    origin.bits[0] = 0b11111111111111111111111111111111;
    origin.bits[1] = 0b11111111111111111111111111111111;
    origin.bits[2] = 0b11111111111111111111111111111111;
    origin.bits[3] = 0b10000000000000000000000000000000;
    ck_assert_int_eq(origin.bits[3], result.bits[3]);
    ck_assert_int_eq(origin.bits[2], result.bits[2]);
    ck_assert_int_eq(origin.bits[1], result.bits[1]);
    ck_assert_int_eq(origin.bits[0], result.bits[0]);
    ck_assert_int_eq(value_type_result, value_type_origin);
  }
}
END_TEST

START_TEST(s21_mod_test) {
  {
    s21_decimal value_1 = {{46, 0, 0, 0}};
    s21_decimal value_2 = {{0, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_mod(value_1, value_2, &result);
    ck_assert_int_eq(return_value, DIV_0);
  }
  {
    s21_decimal value_1 = {{46, 0, 0, 0}};
    s21_decimal value_2 = {{2, 0, 0, 0}};
    s21_decimal check = {{0, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_mod(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{47, 0, 0, 0}};
    s21_decimal value_2 = {{2, 0, 0, 0}};
    s21_decimal check = {{1, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_mod(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{46, 0, 0, 0}};
    s21_decimal value_2 = {{47, 0, 0, 0}};
    s21_decimal check = {{46, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_mod(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{37481, 0, 0, 0}};
    s21_decimal value_2 = {{10, 0, 0, 0}};
    s21_decimal check = {{1, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_mod(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{37481, 0, 0, 0}};
    s21_decimal value_2 = {{100, 0, 0, 0}};
    s21_decimal check = {{81, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_mod(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{37481, 0, 0, 0}};
    s21_decimal value_2 = {{1000, 0, 0, 0}};
    s21_decimal check = {{481, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_mod(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{37481, 0, 0, 0}};
    s21_decimal value_2 = {{10000, 0, 0, 0}};
    s21_decimal check = {{7481, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_mod(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{132, 0, 0, 0}};
    s21_decimal value_2 = {{12, 0, 0, 0}};
    s21_decimal check = {{0, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_mod(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
}
END_TEST

START_TEST(s21_div_test) {
  {
    s21_decimal value_1 = {{46, 0, 0, 0}};
    s21_decimal value_2 = {{0, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_div(value_1, value_2, &result);
    ck_assert_int_eq(return_value, DIV_0);
  }
  {
    s21_decimal value_1 = {{46, 0, 0, 0}};
    s21_decimal value_2 = {{2, 0, 0, 0}};
    s21_decimal check = {{23, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_div(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{46, 0, 0, 0}};
    s21_decimal value_2 = {{47, 0, 0, 0}};
    s21_decimal check = {{0, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_div(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{37481, 0, 0, 0}};
    s21_decimal value_2 = {{2, 0, 0, 0}};
    s21_decimal check = {{18740, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_div(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal value_1 = {{132, 0, 0, 0}};
    s21_decimal value_2 = {{12, 0, 0, 0}};
    s21_decimal check = {{11, 0, 0, 0}};
    s21_decimal result = {0};
    int return_value = s21_div(value_1, value_2, &result);
    ck_assert_int_eq(s21_is_equal(result, check), 1);
    ck_assert_int_eq(return_value, OK);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, result, origin;
    // 30064771176
    // 3
    // 10021590392
    src1.bits[0] = 0b00000000000000000000000001101000;
    src1.bits[1] = 0b00000000000000000000000000000111;
    src1.bits[2] = 0b00000000000000000000000000000000;
    src1.bits[3] = 0b00000000000000000000000000000000;
    src2.bits[0] = 0b00000000000000000000000000000011;
    src2.bits[1] = 0b00000000000000000000000000000000;
    src2.bits[2] = 0b00000000000000000000000000000000;
    src2.bits[3] = 0b00000000000000000000000000000000;
    origin.bits[0] = 0b01010101010101010101010101111000;
    origin.bits[1] = 0b00000000000000000000000000000010;
    origin.bits[2] = 0b00000000000000000000000000000000;
    origin.bits[3] = 0b00000000000000000000000000000000;
    int check = s21_div(src1, src2, &result);
    int check_origin = 0;
    ck_assert_int_eq(check, check_origin);
    ck_assert_int_eq(result.bits[3], origin.bits[3]);
    ck_assert_int_eq(result.bits[2], origin.bits[2]);
    ck_assert_int_eq(result.bits[1], origin.bits[1]);
    ck_assert_int_eq(result.bits[0], origin.bits[0]);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, result = {0};
    int a = 32768;
    int b = -2;
    int res_our_dec = 0;
    s21_from_int_to_decimal(a, &src1);
    s21_from_int_to_decimal(b, &src2);
    int res_origin = -16384;
    int check = s21_div(src1, src2, &result);
    int check_origin = 0;
    s21_from_decimal_to_int(result, &res_our_dec);
    ck_assert_int_eq(res_our_dec, res_origin);
    ck_assert_int_eq(check, check_origin);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, result = {0};
    int a = 32768;
    int b = 2;
    int res_our_dec = 0;
    s21_from_int_to_decimal(a, &src1);
    s21_from_int_to_decimal(b, &src2);
    int res_origin = 16384;
    int check = s21_div(src1, src2, &result);
    int check_origin = 0;
    s21_from_decimal_to_int(result, &res_our_dec);
    ck_assert_int_eq(res_our_dec, res_origin);
    ck_assert_int_eq(check, check_origin);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, result, origin;
    // -30064771176
    // 2
    // 10021590392
    src1.bits[0] = 0b00000000000000000000000001101000;
    src1.bits[1] = 0b00000000000000000000000000000111;
    src1.bits[2] = 0b00000000000000000000000000000000;
    src1.bits[3] = 0b10000000000000000000000000000000;
    src2.bits[0] = 0b00000000000000000000000000000010;
    src2.bits[1] = 0b00000000000000000000000000000000;
    src2.bits[2] = 0b00000000000000000000000000000000;
    src2.bits[3] = 0b00000000000000000000000000000000;
    origin.bits[0] = 0b10000000000000000000000000110100;
    origin.bits[1] = 0b00000000000000000000000000000011;
    origin.bits[2] = 0b00000000000000000000000000000000;
    origin.bits[3] = 0b10000000000000000000000000000000;
    int check = s21_div(src1, src2, &result);
    int check_origin = 0;
    ck_assert_int_eq(check, check_origin);
    ck_assert_int_eq(result.bits[3], origin.bits[3]);
    ck_assert_int_eq(result.bits[2], origin.bits[2]);
    ck_assert_int_eq(result.bits[1], origin.bits[1]);
    ck_assert_int_eq(result.bits[0], origin.bits[0]);
  }
  {
    s21_decimal src1 = {0}, src2 = {0}, result = {0};
    src1.bits[0] = 0x0006BFD0;
    src1.bits[1] = 0x00000000;
    src1.bits[2] = 0x00000000;
    src1.bits[3] = 0x00000000;

    src2.bits[0] = 0x00000028;
    src2.bits[1] = 0x00000000;
    src2.bits[2] = 0x00000000;
    src2.bits[3] = 0x00000000;

    int check = s21_div(src1, src2, &result);
    int check_origin = 0;
    ck_assert_int_eq(check_origin, check);
    ck_assert_int_eq(result.bits[3], 0x00000000);
    ck_assert_int_eq(result.bits[2], 0x00000000);
    ck_assert_int_eq(result.bits[1], 0x00000000);
    ck_assert_int_eq(result.bits[0], 0x00002B32);
  }
}
END_TEST

Suite *s21_arithmetic_suite(void) {
  Suite *s;
  TCase *tc_core;

  s = suite_create("S21 Arithmetic");

  /* Core test case */
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, s21_add_test);
  tcase_add_test(tc_core, s21_sub_test);
  tcase_add_test(tc_core, s21_mul_test);
  tcase_add_test(tc_core, s21_div_test);
  tcase_add_test(tc_core, s21_mod_test);
  suite_add_tcase(s, tc_core);

  return s;
}

int test_arithmetic() {
  int no_failed = 0;
  Suite *s;
  SRunner *sr;

  s = s21_arithmetic_suite();
  sr = srunner_create(s);

  srunner_run_all(sr, CK_NORMAL);
  no_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return no_failed;
}