#include "tests.h"

START_TEST(s21_is_less_test) {
  {
    s21_decimal value_1 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    int return_value = s21_is_less(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }

  {
    s21_decimal value_1 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    value_1.bits[3] |= 1UL << 31;
    int return_value = s21_is_less(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    value_2.bits[3] |= 1UL << 31;
    int return_value = s21_is_less(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    set_exponent(&value_2, 2);
    int return_value = s21_is_less(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }
}
END_TEST

START_TEST(s21_is_less_or_equal_test) {
  {
    s21_decimal value_1 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    int return_value = s21_is_less_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }

  {
    s21_decimal value_1 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    value_1.bits[3] |= 1UL << 31;
    int return_value = s21_is_less_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    value_2.bits[3] |= 1UL << 31;
    int return_value = s21_is_less_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }

  {
    s21_decimal value_1 = {{0, 0, 0, 0}};
    s21_decimal value_2 = {{0, 0, 0, 0}};
    value_2.bits[3] |= 1UL << 31;
    int return_value = s21_is_less_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    value_1.bits[3] |= 1UL << 31;
    value_2.bits[3] |= 1UL << 31;
    int return_value = s21_is_less_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    set_exponent(&value_2, 2);
    int return_value = s21_is_less_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    int return_value = s21_is_less_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }
}
END_TEST

START_TEST(s21_is_greater_test) {
  {
    s21_decimal value_1 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    int return_value = s21_is_greater(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    value_1.bits[3] |= 1UL << 31;
    int return_value = s21_is_greater(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    value_2.bits[3] |= 1UL << 31;
    int return_value = s21_is_greater(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{0, 0, 0, 0}};
    s21_decimal value_2 = {{0, 0, 0, 0}};
    value_2.bits[3] |= 1UL << 31;
    int return_value = s21_is_greater(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    value_1.bits[3] |= 1UL << 31;
    value_2.bits[3] |= 1UL << 31;
    int return_value = s21_is_greater(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    set_exponent(&value_2, 10);
    int return_value = s21_is_greater(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{234, 0, 0, 0}};
    s21_decimal value_2 = {{2, 0, 0, 0}};
    set_exponent(&value_1, 2);
    int return_value = s21_is_greater(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }
}
END_TEST

START_TEST(s21_is_greater_or_equal_test) {
  {
    s21_decimal value_1 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    int return_value = s21_is_greater_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};

    value_1.bits[3] |= 1UL << 31;
    int return_value = s21_is_greater_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    value_2.bits[3] |= 1UL << 31;
    int return_value = s21_is_greater_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{0, 0, 0, 0}};
    s21_decimal value_2 = {{0, 0, 0, 0}};
    value_2.bits[3] |= 1UL << 31;
    int return_value = s21_is_greater_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123457u, 654u, 0xFFFFFFFF, 0}};
    value_1.bits[3] |= 1UL << 31;
    value_2.bits[3] |= 1UL << 31;
    int return_value = s21_is_greater_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    set_exponent(&value_2, 2);
    int return_value = s21_is_greater_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 0}};
    int return_value = s21_is_greater_or_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }
}
END_TEST

START_TEST(s21_is_equal_test) {
  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 80000000}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 80000000}};
    int return_value = s21_is_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{123453u, 654u, 0xFFFFFFFF, 80000000}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 80000000}};
    int return_value = s21_is_equal(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }

  {
    s21_decimal value_1 = {{0, 0, 0, 0}};
    s21_decimal value_2 = {{0, 0, 0, 0x80000000}};
    int return_value = s21_is_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{2, 0, 0, 0}};
    s21_decimal value_2 = {{20, 0, 0, 0}};
    set_exponent(&value_2, 1);
    int return_value = s21_is_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }
  {
    s21_decimal first = {
        {4412, 5675, 1254, 0b00000100000000000010000000000000}};
    s21_decimal second = {
        {4412, 5675, 1254, 0b00000100000000000010000000000000}};
    ck_assert_int_eq(s21_is_equal(first, second), 1);
  }
  {
    s21_decimal first = {
        {4412, 5675, 1254, 0b10000100000000000010000000000000}};
    s21_decimal second = {
        {4412, 5675, 1254, 0b10000100000000000010000000000000}};
    ck_assert_int_eq(s21_is_equal(first, second), 1);
  }
  {
    s21_decimal first = {
        {4412, 5675, 1254, 0b10110010000000001110000111000011}};
    s21_decimal second = {
        {4412, 5675, 1254, 0b11001001000000000010000111111110}};
    ck_assert_int_eq(s21_is_equal(first, second), 1);
  }
  {
    s21_decimal first = {
        {4412, 5675, 1254, 0b10110100111111110010000000000011}};
    s21_decimal second = {
        {4412, 5675, 1254, 0b01110100000000000010000111111110}};
    ck_assert_int_eq(s21_is_equal(first, second), 0);
  }
  {
    s21_decimal first = {
        {4412, 5675, 1254, 0b00110100111111110010000000000011}};
    s21_decimal second = {
        {4412, 5675, 1254, 0b11110100000000000010000111111110}};
    ck_assert_int_eq(s21_is_equal(first, second), 0);
  }
  {
    s21_decimal first = {
        {4412, 5675, 1154, 0b10110100111111110010000000000011}};
    s21_decimal second = {
        {4412, 5675, 1254, 0b11110100000000000010000111111110}};
    ck_assert_int_eq(s21_is_equal(first, second), 0);
  }
  {
    s21_decimal first = {
        {4412, 5675, 1254, 0b10110100111111110010000000000011}};
    s21_decimal second = {
        {4412, 5675, 1244, 0b11110100000000000010000111111110}};
    ck_assert_int_eq(s21_is_equal(first, second), 0);
  }
  {
    s21_decimal first = {
        {4412, 5675, 1254, 0b10110100111111110010000000000011}};
    s21_decimal second = {
        {4412, 5675, 12541, 0b11110100000000000010000111111110}};
    ck_assert_int_eq(s21_is_equal(first, second), 0);
  }
  {
    s21_decimal first = {
        {0x4A1D, 05675, 1254, 0b10110100111111110010000000000011}};
    s21_decimal second = {
        {0x4A1D, 05675, 1254, 0b10110100111111110010000000000011}};
    ck_assert_int_eq(s21_is_equal(first, second), 1);
  }
  {
    s21_decimal first = {{412, 5675, 1254, 0b10110100111111110010000000000011}};
    s21_decimal second = {
        {4412, 5675, 1254, 0b11110100000000000010000111111110}};
    ck_assert_int_eq(s21_is_equal(first, second), 0);
  }
  {
    s21_decimal first = {
        {1564, 31452, 6109410, 0b11110100000000000010000111111110}};
    s21_decimal second = {
        {1564, 31452, 610941, 0b11110100000000000010000111111110}};
    ck_assert_int_eq(s21_is_equal(first, second), 0);
  }
  {
    s21_decimal first = {
        {1564, 31452, 610941, 0b11110100000000000010000111111110}};
    s21_decimal second = {
        {1564, 3142, 610941, 0b11110100000000000010000111111110}};
    ck_assert_int_eq(s21_is_equal(first, second), 0);
  }
  {
    s21_decimal first = {
        {15614, 31452, 610941, 0b11110100000000000010000111111110}};
    s21_decimal second = {
        {1564, 31452, 610941, 0b11110100000000000010000111111110}};
    ck_assert_int_eq(s21_is_equal(first, second), 0);
  }
  {
    s21_decimal src1, src2;
    int origin;
    // src1 = 2;
    // src2 = 2;
    src1.bits[0] = 0b00000000000000000000000000000010;
    src1.bits[1] = 0b00000000000000000000000000000000;
    src1.bits[2] = 0b00000000000000000000000000000000;
    src1.bits[3] = 0b00000000000000000000000000000000;
    src2.bits[0] = 0b00000000000000000000000000000010;
    src2.bits[1] = 0b00000000000000000000000000000000;
    src2.bits[2] = 0b00000000000000000000000000000000;
    src2.bits[3] = 0b00000000000000000000000000000000;
    int result = s21_is_equal(src1, src2);
    origin = 1;
    ck_assert_int_eq(origin, result);
  }
}
END_TEST

START_TEST(s21_is_not_equal_test) {
  {
    s21_decimal value_1 = {{123456u, 654u, 0xFFFFFFFF, 80000000}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 80000000}};
    int return_value = s21_is_not_equal(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }

  {
    s21_decimal value_1 = {{123453u, 654u, 0xFFFFFFFF, 80000000}};
    s21_decimal value_2 = {{123456u, 654u, 0xFFFFFFFF, 80000000}};
    int return_value = s21_is_not_equal(value_1, value_2);
    ck_assert_int_eq(return_value, TRUE);
  }

  {
    s21_decimal value_1 = {{0, 0, 0, 80000000}};
    s21_decimal value_2 = {{0, 0, 0, 0}};
    int return_value = s21_is_not_equal(value_1, value_2);
    ck_assert_int_eq(return_value, FALSE);
  }
}
END_TEST

Suite *s21_comparison_suite(void) {
  Suite *s;
  TCase *tc_core;

  s = suite_create("S21 Comparison");

  /* Core test case */
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, s21_is_less_test);
  tcase_add_test(tc_core, s21_is_less_or_equal_test);
  tcase_add_test(tc_core, s21_is_greater_test);
  tcase_add_test(tc_core, s21_is_greater_or_equal_test);
  tcase_add_test(tc_core, s21_is_equal_test);
  tcase_add_test(tc_core, s21_is_not_equal_test);
  suite_add_tcase(s, tc_core);

  return s;
}

int test_comparison() {
  int no_failed = 0;
  Suite *s;
  SRunner *sr;

  s = s21_comparison_suite();
  sr = srunner_create(s);

  srunner_run_all(sr, CK_NORMAL);
  no_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return no_failed;
}