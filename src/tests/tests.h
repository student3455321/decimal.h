#ifndef TESTS_H_
#define TESTS_H_
#include <check.h>
#include <math.h>

#include "../s21_decimal.h"

int test_arithmetic();
int test_comparison();
int test_conversion();
int test_other();

#endif  // TESTS_H_
