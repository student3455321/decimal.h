#include "s21_decimal.h"

int get_exponent(s21_decimal value) {
  int exponent = 0;
  for (int i = 0; i < 8; i++) {
    exponent += (1UL << i) * (get_a_bit(value, i + 112));
  }
  return exponent;
}

bool get_sign(s21_decimal value) {  // 0 - positive, 1 - negaive
  return value.bits[3] & 1UL << 31;
}

bool get_a_bit(s21_decimal value, int bit_num) {
  int res = 1u << (bit_num % 32);
  return (value.bits[bit_num / 32] & res);
}

void set_bit(s21_decimal *dst, int index, bool bit) {
  int shift = 1u << (index % 32);
  if (bit == 0) {
    dst->bits[index / 32] = dst->bits[index / 32] & (~shift);
  } else {
    dst->bits[index / 32] = dst->bits[index / 32] | shift;
  }
}

void set_exponent(s21_decimal *value, int exp) {
  bool sign = get_sign(*value);
  value->bits[3] &= 0;
  value->bits[3] |= sign << 31;
  value->bits[3] |= exp << 16;
}

void decimal_to_long_decimal(s21_decimal src, s21_long_decimal *dest) {
  dest->bits[6] = src.bits[3];
  for (int i = 0; i < 3; i++) {
    dest->bits[i] = src.bits[i];
  }
}

bool get_a_bit_long(s21_long_decimal value, int bit_num) {
  int res = 1u << (bit_num % 32);
  return (value.bits[bit_num / 32] & res);
}

void set_bit_long(s21_long_decimal *dst, int index, bool bit) {
  int shift = 1u << (index % 32);
  if (bit == 0) {
    dst->bits[index / 32] = dst->bits[index / 32] & (~shift);
  } else {
    dst->bits[index / 32] = dst->bits[index / 32] | shift;
  }
}

bool get_sign_long(s21_long_decimal value) { return value.bits[6] & 1UL << 31; }

int get_exponent_long(s21_long_decimal value) {
  int exponent = 0;
  for (int i = 0; i < 8; i++) {
    exponent += (1UL << i) * (get_a_bit_long(value, i + 112 + 96));
  }
  return exponent;
}

void set_exponent_long(s21_long_decimal *value, int exp) {
  bool sign = get_sign_long(*value);
  value->bits[6] &= 0;
  value->bits[6] |= sign << 31;
  value->bits[6] |= exp << 16;
}

bool check_for_zero_long(s21_long_decimal value) {
  bool ret = TRUE;
  for (int i = 0; i < 6 && ret == TRUE; i++) {
    if (value.bits[i] != 0) {
      ret = FALSE;
    }
  }
  return ret;
}

int long_shift_left(s21_long_decimal *value, int shift_size) {
  if (shift_size == 0) return 0;
  int ret = OK, start_index = 191, shift = shift_size - 1;
  while (get_a_bit_long(*value, start_index) == 0 && start_index >= 0) {
    start_index--;
  }
  if (start_index + shift_size > 191) {
    ret = TOO_BIG;
  } else {
    while (start_index >= 0) {
      set_bit_long(value, shift_size + start_index,
                   get_a_bit_long(*value, start_index));
      start_index--;
    }
    while (shift >= 0) {
      set_bit_long(value, shift, 0);
      shift--;
    }
  }
  return ret;
}

int long_shift_right(s21_long_decimal *value, int shift_size) {
  if (shift_size > 191) {
    return CALC_ERROR;
  }
  for (int i = shift_size, j = 0; i < 192; i++, j++) {
    set_bit_long(value, j, get_a_bit_long(*value, i));
  }
  for (int i = 191 - shift_size; i < 192; i++) {
    set_bit_long(value, i, 0);
  }
  return 0;
}

int bank_round_to_fit(s21_long_decimal *value) {
  int ret = 0;
  int start_index = get_starting_index_long(*value),
      exp = get_exponent_long(*value);
  set_exponent_long(value, 0);
  s21_long_decimal ten = {{10, 0, 0, 0, 0, 0, 0}};
  while (start_index > 95) {
    s21_long_decimal temp = {0};
    long_div(*value, ten, &temp);
    *value = temp;
    start_index = get_starting_index_long(*value);
    exp--;
  }
  if (get_a_bit_long(*value, 0) == 1) {
    s21_long_decimal one = {{1, 0, 0, 0, 0, 0, 0}};
    long_sum(*value, one, value);
  }
  if (get_starting_index_long(*value) > 95) {
    ret = 1;
  }
  if (exp > 0) {
    set_exponent_long(value, exp);
  }
  return ret;
}

int long_decimal_to_decimal(s21_long_decimal src, s21_decimal *dest) {
  int ret = OK;
  s21_long_decimal max_allowed = {0}, src_temp = {0}, min_allowed = {0};
  for (int i = 0; i < 4; i++) {
    dest->bits[i] = 0;
  }
  memcpy(&src_temp, &src, sizeof(s21_long_decimal));
  set_bit_long(&src_temp, 223, 0);  // местный модуль
  for (int i = 0; i < 96; i++) {
    set_bit_long(&max_allowed, i, 1);
  }
  if (long_greater(src_temp, max_allowed) == 1) {
    ret = TOO_BIG + get_sign_long(src);
  }
  set_bit_long(&min_allowed, 0, 1);
  set_exponent_long(&min_allowed, 28);
  if (ret == 0 && long_greater(min_allowed, src_temp) == 1 &&
      !(check_for_zero_long(src_temp))) {
    ret = TOO_SMALL;
  }
  if (ret == OK && (get_starting_index_long(src_temp) > 95)) {
    int bank_round_res = bank_round_to_fit(&src_temp);
    if (bank_round_res == 1) {
      ret = TOO_BIG + get_sign_long(src);
    }
  }
  if (ret == OK) {
    for (int i = 0; i < 3; i++) {
      dest->bits[i] = src.bits[i];
    }
    dest->bits[3] = src.bits[6];
  }
  return ret;
}

void set_to_biggest_exponent(s21_long_decimal *num1, s21_long_decimal *num2) {
  int exp1 = get_exponent_long(*num1), exp2 = get_exponent_long(*num2);
  int diff_exp = abs(exp1 - exp2);
  int target_exp = exp1 > exp2 ? exp1 : exp2;
  s21_long_decimal tens[9] = {
      {{10, 0, 0, 0, 0, 0, 0}},        {{100, 0, 0, 0, 0, 0, 0}},
      {{1000, 0, 0, 0, 0, 0, 0}},      {{10000, 0, 0, 0, 0, 0, 0}},
      {{100000, 0, 0, 0, 0, 0, 0}},    {{1000000, 0, 0, 0, 0, 0, 0}},
      {{10000000, 0, 0, 0, 0, 0, 0}},  {{100000000, 0, 0, 0, 0, 0, 0}},
      {{1000000000, 0, 0, 0, 0, 0, 0}}};
  s21_long_decimal temp_num = {0};
  if (exp1 > exp2) {
    memcpy(&temp_num, num2, sizeof(s21_long_decimal));
    while (diff_exp) {
      if (diff_exp >= 9) {
        mul_long(temp_num, tens[8], num2);
        memcpy(&temp_num, num2, sizeof(s21_long_decimal));
        diff_exp -= 9;
      } else {
        mul_long(temp_num, tens[diff_exp - 1], num2);
        memcpy(&temp_num, num2, sizeof(s21_long_decimal));
        diff_exp = 0;
      }
    }
    set_exponent_long(num2, target_exp);

  } else if (exp1 < exp2) {
    memcpy(&temp_num, num1, sizeof(s21_long_decimal));
    while (diff_exp) {
      if (diff_exp >= 9) {
        mul_long(temp_num, tens[8], num1);
        memcpy(&temp_num, num1, sizeof(s21_long_decimal));
        diff_exp -= 9;
      } else {
        mul_long(temp_num, tens[diff_exp - 1], num1);
        memcpy(&temp_num, num1, sizeof(s21_long_decimal));
        diff_exp = 0;
      }
    }
    set_exponent_long(num1, target_exp);
  }
}

int get_starting_index_long(s21_long_decimal value) {
  int start_index = 191;
  while (get_a_bit_long(value, start_index) == 0 && start_index) {
    start_index--;
  }
  return start_index;
}

int get_starting_index(s21_decimal value) {
  int start_index = 95;
  while (get_a_bit(value, start_index) == 0) {
    start_index--;
  }
  return start_index;
}