#include "s21_decimal.h"

int s21_from_decimal_to_float(s21_decimal src, float *dst) {
  float buff = (float)*dst;
  for (int i = 0; i < 96; i++) {
    buff += get_a_bit(src, i) * pow(2, i);
  }
  buff = buff * pow(10, (-1 * get_exponent(src)));
  if (get_a_bit(src, 127)) {
    buff = buff * (-1);
  }
  *dst = buff;
  return 0;
}