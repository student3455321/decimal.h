#include "s21_decimal.h"

int long_greater(s21_long_decimal num1,
                 s21_long_decimal num2) {  // сравнивает только мантиссы
  int ret = TRUE;
  set_to_biggest_exponent(&num1, &num2);
  int index = 191;
  while (get_a_bit_long(num1, index) == get_a_bit_long(num2, index) &&
         index != -1) {
    index--;
  }
  if (index == -1) {
    ret = FALSE;
  } else {
    ret = get_a_bit_long(num1, index) ^ get_sign_long(num1);
  }
  return ret;
}

int s21_is_greater(s21_decimal value_1, s21_decimal value_2) {
  int ret = TRUE;
  int sign1 = get_sign(value_1), sign2 = get_sign(value_2);
  if (sign1 == 1 &&
      sign2 == 0) {  //  первое число отрицательное, второе положительное
    ret = FALSE;
  }
  if (ret == TRUE && s21_is_equal(value_1, value_2) == 1) {
    ret = FALSE;
  }
  if (ret == TRUE && sign1 == sign2) {
    s21_long_decimal num1 = {0}, num2 = {0};
    decimal_to_long_decimal(value_1, &num1);
    decimal_to_long_decimal(value_2, &num2);
    ret = long_greater(num1, num2);
  }
  return ret;
}