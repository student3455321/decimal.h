#include "s21_decimal.h"

int s21_from_int_to_decimal(int src, s21_decimal *dst) {
  s21_decimal dst_zero = {{0, 0, 0, 0}};
  *dst = dst_zero;
  int error = 0;
  if ((src == S21_NAN || src == S21_POS_INF || src == S21_NEG_INF) ||
      dst == NULL) {
    return CONVERT_ERROR;
  }
  if (src) {
    if (src < 0) {
      set_bit(dst, 127, 1);
      src = src * -1;
    }
    dst->bits[0] = src;
  }
  return error;
}
