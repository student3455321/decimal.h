#include "s21_decimal.h"

int mul_long(s21_long_decimal value1, s21_long_decimal value2,
             s21_long_decimal *result) {
  for (int i = 0; i < 7; i++) {
    result->bits[i] = 0;
  }
  if (!(long_eq(value1, *result) || long_eq(value2, *result))) {
    int last_shifted_index = 0, exp1 = get_exponent_long(value1),
        exp2 = get_exponent_long(value2);
    set_exponent_long(&value1, 0);
    set_exponent_long(&value2, 0);
    s21_long_decimal val1_to_shift = {0};
    memcpy(&val1_to_shift, &value1, sizeof(s21_long_decimal));
    for (int i = 0; i < 96; i++) {
      if (get_a_bit_long(value2, i) == 1) {
        long_shift_left(&val1_to_shift, i - last_shifted_index);
        long_sum(val1_to_shift, *result, result);
        last_shifted_index = i;
      }
    }
    set_exponent_long(result, exp1 + exp2);
    set_bit_long(result, 223, get_sign_long(value1) ^ get_sign_long(value2));
  }
  return 0;
}

int s21_mul(s21_decimal value_1, s21_decimal value_2, s21_decimal *result) {
  s21_long_decimal num1 = {0}, num2 = {0}, result_long = {0};
  s21_decimal zero = {0};
  int ret = 0;
  *result = zero;
  int sign1 = get_sign(value_1), sign2 = get_sign(value_2);
  if (!(s21_is_equal(value_1, zero) || s21_is_equal(value_2, zero))) {
    decimal_to_long_decimal(value_1, &num1);
    decimal_to_long_decimal(value_2, &num2);
    set_bit_long(&num1, 227, 0);
    set_bit_long(&num2, 227, 0);
    mul_long(num1, num2, &result_long);
    ret = long_decimal_to_decimal(result_long, result);
    set_bit(result, 127, sign1 ^ sign2);
  }
  return ret;
}
