#include "s21_decimal.h"

int long_eq(s21_long_decimal value_1, s21_long_decimal value_2) {
  if (get_exponent_long(value_1) != get_exponent_long(value_2)) {
    set_to_biggest_exponent(&value_1, &value_2);
  }
  bool ret = TRUE;
  bool bit1 = 0, bit2 = 0, is_zero = 1;
  for (int i = 0; i < 96 && ret == TRUE; i++) {
    bit1 = get_a_bit_long(value_1, i);
    bit2 = get_a_bit_long(value_2, i);
    if (is_zero && (bit1 || bit2)) {
      is_zero = 0;
    }
    if (bit1 != bit2) {
      ret = FALSE;
    }
  }
  if (is_zero == 0 && ret) {
    ret = (get_sign_long(value_1) == get_sign_long(value_2));
  }
  return ret;
}

int s21_is_equal(s21_decimal value_1, s21_decimal value_2) {
  s21_long_decimal num1 = {0}, num2 = {0};
  decimal_to_long_decimal(value_1, &num1);
  decimal_to_long_decimal(value_2, &num2);
  int return_value = long_eq(num1, num2);
  return return_value;
}