#include "s21_decimal.h"

union conv {
  float fl;
  int digit;
};

int s21_from_float_to_decimal(float src, s21_decimal *dst) {
  if (src == S21_NAN || src == S21_NEG_INF || src == S21_POS_INF ||
      dst == NULL) {
    return CONVERT_ERROR;
  } else {
    s21_decimal dst_zero = {{0, 0, 0, 0}};
    *dst = dst_zero;
    union conv zhopa;
    zhopa.fl = src;
    int exp = (((255 << 23) & zhopa.digit) >> 23) - 127;
    if (exp > 95 || exp < -94) {
      return CONVERT_ERROR;
    }
    if (exp < 95) {
      int scale = 0;
      for (; !((int)src); src *= 10, scale++) {
      }  //нормализация, здесь мы получаем первый ненулевой символ
      src *= 10000000;
      scale += 7;
      while (fmod(src, 10) == 0) {
        src /= 10;
        scale--;
      }
      zhopa.fl = src;
      exp = (((255 << 23) & zhopa.digit) >> 23) - 127;
      unsigned int mask = 0x400000;
      set_bit(dst, exp, 1);
      for (int bit_idx = exp - 1; mask; mask >>= 1, bit_idx--) {
        set_bit(dst, bit_idx, !!(zhopa.digit & mask));
      }
      if (src < 0) {
        set_bit(dst, 127, 1);
      }
      set_exponent(dst, scale);
    }
  }
  return OK;
}