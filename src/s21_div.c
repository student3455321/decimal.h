#include "s21_decimal.h"

int s21_div(s21_decimal value_1, s21_decimal value_2, s21_decimal *result) {
  s21_decimal zero = {0};
  *result = zero;
  if (s21_is_equal(value_2, zero) == 1) {
    return DIV_0;
  }
  int sign1 = get_sign(value_1), sign2 = get_sign(value_2);
  set_bit(&value_1, 127, 0);
  set_bit(&value_2, 127, 0);
  s21_long_decimal num1 = {0}, num2 = {0}, result_long = {0};
  decimal_to_long_decimal(value_1, &num1);
  decimal_to_long_decimal(value_2, &num2);
  long_div(num1, num2, &result_long);
  int ret = long_decimal_to_decimal(result_long, result);
  if (ret) {
    *result = zero;
  } else {
    set_bit(result, 127, sign1 ^ sign2);
  }
  return ret;
}

// 1101010 11111110 11111001 11101000