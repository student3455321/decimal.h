#include "s21_decimal.h"

int s21_round(s21_decimal value, s21_decimal *result) {
  int exp = get_exponent(value);
  int sign = get_sign(value);
  if (exp) {
    s21_decimal temp = value;
    set_bit(&temp, 127, 0);
    s21_decimal zero = {{0, 0, 0, 0}};
    set_exponent(&temp, 0);
    s21_decimal tens[9] = {
        {{10, 0, 0, 0}},       {{100, 0, 0, 0}},       {{1000, 0, 0, 0}},
        {{10000, 0, 0, 0}},    {{100000, 0, 0, 0}},    {{1000000, 0, 0, 0}},
        {{10000000, 0, 0, 0}}, {{100000000, 0, 0, 0}}, {{1000000000, 0, 0, 0}}};
    while ((exp > 1) && s21_is_equal(temp, zero) == 0) {
      if (exp >= 10) {
        s21_div(temp, tens[8], result);
        memcpy(&temp, result, sizeof(s21_decimal));
        exp -= 9;
      } else {
        s21_div(temp, tens[exp - 2], result);
        memcpy(&temp, result, sizeof(s21_decimal));
        exp = 1;
      }
    }
    s21_decimal zheppa = {0};
    s21_decimal five = {{5, 0, 0, 0}};
    s21_mod(temp, tens[0], &zheppa);
    if (s21_is_greater_or_equal(zheppa, five)) {
      s21_add(temp, tens[0], &temp);
    }
    s21_div(temp, tens[0], result);
    if (sign == 1) {
      set_bit(result, 127, 1);
    }
  } else {
    memcpy(result, &value, sizeof(s21_decimal));
  }
  return 0;
}
