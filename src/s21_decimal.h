#ifndef S21_DECIMAL_H_
#define S21_DECIMAL_H_

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INT_MAX 2147483647

#define S21_NAN 0.0 / 0.0
#define S21_POS_INF 1.0 / 0.0
#define S21_NEG_INF -1.0 / 0.0

#define OK 0
#define TOO_BIG 1
#define TOO_SMALL 2
#define DIV_0 3

#define FALSE 0
#define TRUE 1

#define CONVERT_ERROR 1  // for converters
#define CALC_ERROR 1     // for other functions

typedef struct {
  unsigned int bits[4];  // bit[3] - управляющий
} s21_decimal;

typedef struct {
  unsigned int bits[7];  // bit[6] - управляющий
} s21_long_decimal;

/* converters */
int s21_from_int_to_decimal(int src, s21_decimal *dst);
int s21_from_float_to_decimal(float src, s21_decimal *dst);
int s21_from_decimal_to_int(s21_decimal src, int *dst);
int s21_from_decimal_to_float(s21_decimal src, float *dst);

/*comparison*/
int s21_is_equal(s21_decimal value_1, s21_decimal value_2);
int s21_is_greater(s21_decimal value_1, s21_decimal value_2);

int s21_is_less(s21_decimal value_1, s21_decimal value_2);
int s21_is_greater_or_equal(s21_decimal value_1, s21_decimal value_2);
int s21_is_not_equal(s21_decimal value_1, s21_decimal value_2);
int s21_is_less_or_equal(s21_decimal value_1, s21_decimal value_2);

/*number manipulation*/
int s21_floor(s21_decimal value, s21_decimal *result);
int s21_round(s21_decimal value, s21_decimal *result);
int s21_truncate(s21_decimal value, s21_decimal *result);
int s21_negate(s21_decimal value, s21_decimal *result);

/*arithmetic operations*/
int s21_add(s21_decimal value_1, s21_decimal value_2, s21_decimal *result);
int s21_sub(s21_decimal value_1, s21_decimal value_2,
            s21_decimal *result);  // нужны сравнения потому что хотим вычитать
                                   // из большего меньшее
int s21_mul(s21_decimal value_1, s21_decimal value_2, s21_decimal *result);
int s21_div(s21_decimal value_1, s21_decimal value_2, s21_decimal *result);
int s21_mod(s21_decimal value_1, s21_decimal value_2, s21_decimal *result);

/*utility functions*/
bool get_a_bit(s21_decimal value, int bit_num);
int get_exponent(s21_decimal value);
bool get_sign(s21_decimal value);
bool bit_comparison(
    s21_decimal value_1,
    s21_decimal value_2);  // сравнивает мантисы чисел, возвращает 1, если
                           // мантиса1 > мантса2
void set_bit(s21_decimal *dst, int index, bool bit);
void set_exponent(s21_decimal *value, int exp);
void print_base2(unsigned int num1);
int get_starting_index(s21_decimal value);

/* long utility functions*/
void decimal_to_long_decimal(s21_decimal src, s21_long_decimal *dest);
int long_decimal_to_decimal(s21_long_decimal src, s21_decimal *dest);
bool get_a_bit_long(s21_long_decimal value, int bit_num);
int get_exponent_long(s21_long_decimal value);
bool get_sign_long(s21_long_decimal value);
void set_bit_long(s21_long_decimal *dst, int index, bool bit);
void set_exponent_long(s21_long_decimal *value, int exp);
bool check_for_zero_long(s21_long_decimal value);
int long_shift_left(s21_long_decimal *value, int shift_size);
int long_shift_right(s21_long_decimal *value, int shift_size);
int long_sum(s21_long_decimal value1, s21_long_decimal value2,
             s21_long_decimal *result);
int long_sub(s21_long_decimal num1, s21_long_decimal num2,
             s21_long_decimal *result);
int mul_long(s21_long_decimal value1, s21_long_decimal value2,
             s21_long_decimal *result);
int long_greater(s21_long_decimal num1, s21_long_decimal num2);
void set_to_biggest_exponent(s21_long_decimal *num1, s21_long_decimal *num2);
int long_eq(s21_long_decimal value_1, s21_long_decimal value_2);
int get_starting_index_long(s21_long_decimal value);
int long_div(s21_long_decimal num1, s21_long_decimal num2,
             s21_long_decimal *result);
#endif  // S21_DECIMAL_H_