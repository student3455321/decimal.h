#include "s21_decimal.h"

int long_sub(s21_long_decimal num1, s21_long_decimal num2,
             s21_long_decimal *result) {
  if (get_exponent_long(num1) != get_exponent_long(num2)) {
    set_to_biggest_exponent(&num1, &num2);
  }
  int start_index = 191;
  while (get_a_bit_long(num1, start_index) == 0) {
    start_index--;
  }
  s21_long_decimal complete_to_one = {0};
  // создаем дополнение до единицы
  for (int i = start_index; i >= 0; i--) {
    set_bit_long(&complete_to_one, i, 1);
  }
  for (int i = 0; i < 6; i++) {
    complete_to_one.bits[i] ^= num2.bits[i];
  }
  s21_long_decimal added = {0}, one = {0}, subtracted = {0};
  one.bits[0] = 1;
  set_exponent_long(&complete_to_one, get_exponent_long(num1));
  set_exponent_long(&one, get_exponent_long(num1));
  long_sum(num1, complete_to_one, &added);
  long_sum(added, one, &added);
  set_bit_long(&subtracted, start_index + 1, 1);
  for (int i = 0; i < 6; i++) {
    added.bits[i] ^= subtracted.bits[i];
  }
  memcpy(result, &added, sizeof(s21_long_decimal));
  return 0;
}

int s21_sub(s21_decimal value_1, s21_decimal value_2, s21_decimal *result) {
  int sign1 = get_sign(value_1), sign2 = get_sign(value_2);
  bool need_to_change_sign = 0;
  s21_long_decimal num1 = {0}, num2 = {0}, result_long = {0};
  decimal_to_long_decimal(value_1, &num1);
  set_bit_long(&num1, 223, 0);
  decimal_to_long_decimal(value_2, &num2);
  set_bit_long(&num2, 223, 0);
  bool first_greater_second = long_greater(num1, num2);
  // добавить гучи свапы, чтобы по возможности вычитать из большего меньшее
  if (sign1 != sign2) {
    long_sum(num1, num2, &result_long);
    need_to_change_sign = sign1;
  } else {
    if (first_greater_second == 1) {
      long_sub(num1, num2, &result_long);
    } else {
      long_sub(num2, num1, &result_long);
    }
    need_to_change_sign = sign1 == first_greater_second;
  }
  if (need_to_change_sign) {
    set_bit_long(&result_long, 223, 1);
  }
  int ret = long_decimal_to_decimal(result_long, result);
  return ret;
}
